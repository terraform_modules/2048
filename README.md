# 2048 Demo Module

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| backup |  | string | `"Backup"` | no |
| domain |  | string | n/a | yes |
| logging\_bucket | ALB Variables | string | `""` | no |
| logging\_enabled |  | string | `"true"` | no |
| public\_subnets | List of public subnets to deploy ec2 instance in | list | n/a | yes |
| region |  | string | n/a | yes |
| security\_groups | List of Security Groups | string | n/a | yes |
| ssh\_key | SSH Key for EC2 Instances | string | n/a | yes |
| vpc\_id | ID of VPC | string | n/a | yes |
| vpc\_s3\_key |  | string | n/a | yes |

 <!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
