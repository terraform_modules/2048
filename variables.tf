variable "region" {}

# variable "security_groups" {
#   type = "list"
# }

variable "backup" {
  default = "Backup"
}

#ALB Variables
variable "logging_bucket" {
  default = ""
}

variable "logging_enabled" {
  default = "true"
}

variable "domain" {}

variable "vpc_s3_key" {}

variable "vpc_id" {
  description = "ID of VPC"
}

variable "security_groups" {
  description = "List of Security Groups"
}

variable "public_subnets" {
  description = "List of public subnets to deploy ec2 instance in"
  type        = "list"
}

variable "ssh_key" {
  description = "SSH Key for EC2 Instances"
}
